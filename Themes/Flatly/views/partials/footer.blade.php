<footer class="container">
    <hr/>
    <div class="row">
        <div class="col-md-12">
            @menu('main')

        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            © 2018 by Happiness Consulting
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                </div>

                <div class="col-md-6 text-right">
                    <ul aria-label="Social bar" id="comp-j56p2yggitemsContainer" class="lb1itemsContainer">
                        <li style="width:39px;height:39px;margin-bottom:0;margin-right:14px;display:inline-block"
                            class="lb1imageItem" id="comp-j56p2ygg0image"><a href="https://www.linkedin.com/groups/13538123"
                                                                             target="_blank"
                                                                             data-content="https://www.linkedin.com/groups/13538123"
                                                                             data-type="external" id="comp-j56p2ygg0imagelink"
                                                                             class="lb1imageItemlink">
                                <div data-image-info="{&quot;imageData&quot;:{&quot;link&quot;:{&quot;type&quot;:&quot;ExternalLink&quot;,&quot;id&quot;:&quot;dataItem-j56p94y5&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;url&quot;:&quot;https://www.linkedin.com/groups/13538123&quot;,&quot;target&quot;:&quot;_blank&quot;},&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-j56p94y4&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;LinkedIn Social Icon&quot;,&quot;uri&quot;:&quot;48a2a42b19814efaa824450f23e8a253.png&quot;,&quot;width&quot;:200,&quot;height&quot;:200,&quot;alt&quot;:&quot;LinkedIn Social Icon&quot;},&quot;displayMode&quot;:&quot;fill&quot;}"
                                     style="width: 39px; height: 39px; position: absolute;"
                                     data-style="position:absolute;width:39;height:39" class="lb1imageItemimage"
                                     id="comp-j56p2ygg0imageimage"><img id="comp-j56p2ygg0imageimageimage"
                                                                        alt="LinkedIn Social Icon" data-type="image"
                                                                        style="width: 39px; height: 39px; object-fit: cover;"
                                                                        src="https://static.wixstatic.com/media/48a2a42b19814efaa824450f23e8a253.png/v1/fill/w_39,h_39,al_c,usm_0.66_1.00_0.01/48a2a42b19814efaa824450f23e8a253.png">
                                </div>
                            </a></li>
                        <li style="width:39px;height:39px;margin-bottom:0;margin-right:14px;display:inline-block"
                            class="lb1imageItem" id="comp-j56p2ygg1image"><a href="https://www.linkedin.com/in/sealfodor/"
                                                                             target="_blank"
                                                                             data-content="https://www.linkedin.com/in/sealfodor/"
                                                                             data-type="external" id="comp-j56p2ygg1imagelink"
                                                                             class="lb1imageItemlink">
                                <div data-image-info="{&quot;imageData&quot;:{&quot;link&quot;:{&quot;type&quot;:&quot;ExternalLink&quot;,&quot;id&quot;:&quot;dataItem-j56p94y7&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;url&quot;:&quot;https://www.linkedin.com/in/sealfodor/&quot;,&quot;target&quot;:&quot;_blank&quot;},&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-j56p94y6&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;LinkedIn - Grey Circle&quot;,&quot;uri&quot;:&quot;2118d8383c3e40f98615a57c1fe12d70.png&quot;,&quot;width&quot;:200,&quot;height&quot;:200,&quot;alt&quot;:&quot;LinkedIn - Grey Circle&quot;},&quot;displayMode&quot;:&quot;fill&quot;}"
                                     style="width: 39px; height: 39px; position: absolute;"
                                     data-style="position:absolute;width:39;height:39" class="lb1imageItemimage"
                                     id="comp-j56p2ygg1imageimage"><img id="comp-j56p2ygg1imageimageimage"
                                                                        alt="LinkedIn - Grey Circle" data-type="image"
                                                                        style="width: 39px; height: 39px; object-fit: cover;"
                                                                        src="https://static.wixstatic.com/media/2118d8383c3e40f98615a57c1fe12d70.png/v1/fill/w_39,h_39,al_c,usm_0.66_1.00_0.01/2118d8383c3e40f98615a57c1fe12d70.png">
                                </div>
                            </a></li>
                        <li style="width:39px;height:39px;margin-bottom:0;margin-right:14px;display:inline-block"
                            class="lb1imageItem" id="comp-j56p2ygg2image"><a
                                    href="https://www.facebook.com/HappinessConsulting/" target="_blank"
                                    data-content="https://www.facebook.com/HappinessConsulting/" data-type="external"
                                    id="comp-j56p2ygg2imagelink" class="lb1imageItemlink">
                                <div data-image-info="{&quot;imageData&quot;:{&quot;link&quot;:{&quot;type&quot;:&quot;ExternalLink&quot;,&quot;id&quot;:&quot;dataItem-j56p2yh81&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;url&quot;:&quot;https://www.facebook.com/HappinessConsulting/&quot;,&quot;target&quot;:&quot;_blank&quot;},&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-j56p2yh8&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;Facebook Social Icon&quot;,&quot;uri&quot;:&quot;e316f544f9094143b9eac01f1f19e697.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:200,&quot;height&quot;:200,&quot;alt&quot;:&quot;Facebook Social Icon&quot;},&quot;displayMode&quot;:&quot;fill&quot;}"
                                     style="width: 39px; height: 39px; position: absolute;"
                                     data-style="position:absolute;width:39;height:39" class="lb1imageItemimage"
                                     id="comp-j56p2ygg2imageimage"><img id="comp-j56p2ygg2imageimageimage"
                                                                        alt="Facebook Social Icon" data-type="image"
                                                                        style="width: 39px; height: 39px; object-fit: cover;"
                                                                        src="https://static.wixstatic.com/media/e316f544f9094143b9eac01f1f19e697.png/v1/fill/w_39,h_39,al_c,usm_0.66_1.00_0.01/e316f544f9094143b9eac01f1f19e697.png">
                                </div>
                            </a></li>
                        <li style="width:39px;height:39px;margin-bottom:0;margin-right:14px;display:inline-block"
                            class="lb1imageItem" id="comp-j56p2ygg3image"><a
                                    href="https://www.facebook.com/Happiness-for-All-413808118633195/" target="_blank"
                                    data-content="https://www.facebook.com/Happiness-for-All-413808118633195/"
                                    data-type="external" id="comp-j56p2ygg3imagelink" class="lb1imageItemlink">
                                <div data-image-info="{&quot;imageData&quot;:{&quot;link&quot;:{&quot;type&quot;:&quot;ExternalLink&quot;,&quot;id&quot;:&quot;dataItem-j56p94y91&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;url&quot;:&quot;https://www.facebook.com/Happiness-for-All-413808118633195/&quot;,&quot;target&quot;:&quot;_blank&quot;},&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-j56p94y9&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;Facebook - Grey Circle&quot;,&quot;uri&quot;:&quot;72a44c1b3b7c450287b9f4dd21be1548.png&quot;,&quot;width&quot;:200,&quot;height&quot;:200,&quot;alt&quot;:&quot;Facebook - Grey Circle&quot;},&quot;displayMode&quot;:&quot;fill&quot;}"
                                     style="width: 39px; height: 39px; position: absolute;"
                                     data-style="position:absolute;width:39;height:39" class="lb1imageItemimage"
                                     id="comp-j56p2ygg3imageimage"><img id="comp-j56p2ygg3imageimageimage"
                                                                        alt="Facebook - Grey Circle" data-type="image"
                                                                        style="width: 39px; height: 39px; object-fit: cover;"
                                                                        src="https://static.wixstatic.com/media/72a44c1b3b7c450287b9f4dd21be1548.png/v1/fill/w_39,h_39,al_c,usm_0.66_1.00_0.01/72a44c1b3b7c450287b9f4dd21be1548.png">
                                </div>
                            </a></li>
                        <li style="width:39px;height:39px;margin-bottom:0;margin-right:14px;display:inline-block"
                            class="lb1imageItem" id="comp-j56p2ygg4image"><a
                                    href="https://www.youtube.com/channel/UCMw0BPBfKueF56AjTBEomSQ" target="_blank"
                                    data-content="https://www.youtube.com/channel/UCMw0BPBfKueF56AjTBEomSQ" data-type="external"
                                    id="comp-j56p2ygg4imagelink" class="lb1imageItemlink">
                                <div data-image-info="{&quot;imageData&quot;:{&quot;link&quot;:{&quot;type&quot;:&quot;ExternalLink&quot;,&quot;id&quot;:&quot;dataItem-j56p94ya&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;url&quot;:&quot;https://www.youtube.com/channel/UCMw0BPBfKueF56AjTBEomSQ&quot;,&quot;target&quot;:&quot;_blank&quot;},&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-j56p2yh9&quot;,&quot;metaData&quot;:{&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;YouTube Social  Icon&quot;,&quot;uri&quot;:&quot;a1b09fe8b7f04378a9fe076748ad4a6a.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:200,&quot;height&quot;:200,&quot;alt&quot;:&quot;YouTube Social  Icon&quot;},&quot;displayMode&quot;:&quot;fill&quot;}"
                                     style="width: 39px; height: 39px; position: absolute;"
                                     data-style="position:absolute;width:39;height:39" class="lb1imageItemimage"
                                     id="comp-j56p2ygg4imageimage"><img id="comp-j56p2ygg4imageimageimage"
                                                                        alt="YouTube Social  Icon" data-type="image"
                                                                        style="width: 39px; height: 39px; object-fit: cover;"
                                                                        src="https://static.wixstatic.com/media/a1b09fe8b7f04378a9fe076748ad4a6a.png/v1/fill/w_39,h_39,al_c,usm_0.66_1.00_0.01/a1b09fe8b7f04378a9fe076748ad4a6a.png">
                                </div>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12 text-center ">
            @menu('GDPR')

        </div>
    </div>

</footer>
